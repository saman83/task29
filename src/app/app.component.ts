import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';
  public message: string = 'Hello! I am from typeScript'
  public decimal: number = 6;
  public list: number[] = [1, 2, 3];
  public reminders: string[] = [
    'learn TypeScript',
    'Complete task29',
    'Cure Corona'
  ];
  public data: any = {
    score: 20000,
    player: 'Saman',
    gameover: false 
  };

  info = [
    {name: 'Mohammed', surname: 'Salah', age: 27},
    {name: 'Sadio', surname: 'Mane', age: 28},
    {name: 'Zinedine', surname: 'Zidane', age: 47},
    {name: 'Paul', surname: 'Pogba', age: 27},
    {name: 'Riyad', surname: 'Mahrez', age: 29},
  ];
}

